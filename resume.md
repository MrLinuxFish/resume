# Dustin Bosveld #
(970)430-8039 - DustinBosveld@gmail.com

## Profile: ##

I'm passionate about: excellence, learning, and launching people to reach their full potential. I will not half do a job and I insist on doing things right the first time. I am constantly learning new skills. And, most importantly, I constantly work to promote people to their full potential.

## Experience: ##

**Area Manager**: WIS, December 2020 - Present

- Manage the local team of people
- Provide timely and accurate information to customers
- Train new hire employees to be effective at the job

**Inventory Supervisor**: RGIS/WIS, March 2017 - December 2020

- Lead inventory crews deliver accurate and efficient inventories
- Prepare for inventories through direct communication with customers
- Produce reports on inventories with high accuracy and efficiently

**Youth Leader**: Rez.church January 2017 - March 2020

- Mentor and lead groups middle school students

**Mail Shop Clerk**: Front Range Enterprises and Communication, February 2015 - October 2016

- Produce bulk mail by operating specialized equipment
- Troubleshoot and repair bulk mail machines
- Drive finalized bulk mail to the post office

**Customer Service Representative**: Qualfon, August 2012 - February 2015

- Answer L1 Technical support calls for TiVo
- Troubleshoot TiVo devices in a timely manner

**Technology Intern**: Eaton School District Re2, August 2010 - May 2012

- Troubleshoot and repair faulty computers
- Perform software installations and upgrades on production machines
- Image around 1000 computers for deployment
- Manage Linux installations

## Education: ##

- **Leadership school**: RSM, 3 years training
- **Technical school**: Front Range Community College, Network+ and A+ Certification courses
- **High School**: Eaton High School, Graduate

## Job Skills: ##

- Proficient at leading large groups of people
- Able to follow instructions
- Adaptable
- Good at optimizing workflows
- Proficient at typing
- Exceptional memory
- Troubleshooting

## References: ##

**Greg Brown**, Leadership school director and mentor: (720) 442-2348

**Isaiah Hupp**, Pastor: (970) 573-8186
